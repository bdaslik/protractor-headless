  
FROM node:10.23.0-stretch-slim
WORKDIR /tmp
COPY protractor.sh /
COPY package.json /modules/
COPY environment /etc/sudoers.d/
RUN chmod 777 /protractor.sh
RUN apt-get update && apt-get install -y xvfb wget sudo
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list && \
    wget https://dl.google.com/linux/linux_signing_key.pub && \
    apt-get install -y gnupg2 && \
    apt-key add linux_signing_key.pub && \
    apt-get update && \
    apt-get install -y google-chrome-stable
RUN npm install -g protractor && \
    npm install /modules/ && \
    node /usr/local/lib/node_modules/protractor/bin/webdriver-manager update --gecko=false --versions.chrome=$(google-chrome --version | cut -d ' ' -f 3)
RUN apt-get install -f -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir /protractor
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null SCREEN_RES=1920x1080x24
WORKDIR /protractor
ENTRYPOINT ["/protractor.sh"]